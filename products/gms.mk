#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# PrebuiltGmsCore
PRODUCT_PACKAGES += \
    PrebuiltGmsCore

# GMS
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleContacts \
    GoogleDialer \
    LatinIMEGooglePrebuilt \
    NexusLauncherRelease \
    Phonesky \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    SettingsIntelligenceGooglePrebuilt \
    TurboPrebuilt \
    Velvet

# Artifact path requirement allowlist
PRODUCT_ARTIFACT_PATH_REQUIREMENT_ALLOWED_LIST += \
    system/app/GoogleExtShared/GoogleExtShared.apk \
    system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk \
    system/etc/permissions/privapp-permissions-google.xml \
    system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk \
    system/priv-app/GooglePackageInstaller/GooglePackageInstaller.apk \
    system/priv-app/TagGoogle/TagGoogle.apk

# arcore
TARGET_INCLUDE_STOCK_ARCORE ?= true
ifeq ($(TARGET_INCLUDE_STOCK_ARCORE),true)
PRODUCT_PACKAGES += \
    arcore
endif

# Live Wallpapers
TARGET_INCLUDE_LIVE_WALLPAPERS ?= false
ifeq ($(TARGET_INCLUDE_LIVE_WALLPAPERS),true)
PRODUCT_PACKAGES += \
    PixelWallpapers2023 \
    PixelLiveWallpaperPrebuilt
endif

# Quick Tap
TARGET_SUPPORTS_QUICK_TAP ?= true
ifeq ($(TARGET_SUPPORTS_QUICK_TAP),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/quick_tap.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/quick_tap.xml
endif

# Call recording on Google Dialer
TARGET_SUPPORTS_CALL_RECORDING ?= true
ifeq ($(TARGET_SUPPORTS_CALL_RECORDING),true)
PRODUCT_PACKAGES += \
    com.google.android.apps.dialer.call_recording_audio.features

PRODUCT_PACKAGES += \
    GoogleDialerConfOverlay
endif

# Dex preopt
PRODUCT_DEXPREOPT_SPEED_APPS += \
    NexusLauncherRelease

# Dex preopt profiles
PRODUCT_DEX_PREOPT_PROFILE_DIR := vendor/google/dexopt_profiles/common

ifeq ($(TARGET_SUPPORTS_GOOGLE_BATTERY),true)
PRODUCT_PACKAGES += \
    TurboAdapter

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/system_ext/lib64/libpowerstatshaldataprovider.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/lib64/libpowerstatshaldataprovider.so

PRODUCT_PACKAGES += \
    LibPowerStatsSymLink
endif

ifeq ($(TARGET_SUPPORTS_ADPATIVE_CHARGING),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/adaptivecharging.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/adaptivecharging.xml
endif

ifeq ($(TARGET_SUPPORTS_GOOGLE_CAMERA),true)
PRODUCT_PACKAGES += \
    GoogleCamera

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/GoogleCamera_6gb_or_more_ram.xml
endif

ifeq ($(TARGET_SUPPORTS_DREAMLINER),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/dreamliner.xml \
    vendor/google/gms/common/proprietary/product/etc/permissions/com.google.android.apps.dreamliner.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/com.google.android.apps.dreamliner.xml

PRODUCT_PACKAGES += \
    DreamlinerPrebuilt \
    DreamlinerUpdater
endif

PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2022_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2022_midyear.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2021.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2021.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2023.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2023.xml \
    vendor/google/gms/common/proprietary/product/etc/sysconfig/pixel_experience_2023_midyear.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/pixel_experience_2023_midyear.xml

ifeq ($(TARGET_PIXEL_TABLET_EXPERIENCE_2023),true)
PRODUCT_PACKAGES += \
    pixel_tablet_experience_2023 \
    pixel_tablet-initial-package-stopped-states
endif

$(call inherit-product, vendor/google/gms/common/common-vendor.mk)

# Properties
$(call inherit-product, vendor/google/gms/products/properties.mk)

# Pixel Framework
ifneq ($(TARGET_INCLUDE_PIXEL_FRAMEWORKS),true)
PRODUCT_COPY_FILES += \
    vendor/google/gms/common/proprietary/system_ext/etc/permissions/com.google.android.settings.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.google.android.settings.xml \
    vendor/google/gms/common/proprietary/system_ext/etc/permissions/com.google.android.systemui.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/com.google.android.systemui.xml
endif

# Customizations
$(call inherit-product, vendor/google/gms/products/custom.mk)
